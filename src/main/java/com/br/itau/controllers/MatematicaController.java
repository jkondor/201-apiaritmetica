package com.br.itau.controllers;

import com.br.itau.models.Matematica;
import com.br.itau.services.MatematicaService;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("soma")
    public Integer soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();

        if (numeros.isEmpty() || numeros.size()<2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números para serem somados");
        }
        return matematicaService.soma(numeros);
    }

    @PostMapping("subtracao")
    public Integer subtracao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();

        if (numeros.isEmpty() || numeros.size()<2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números para serem subtraidos");
        }
        return matematicaService.subtrai(numeros);
    }

    @PostMapping("multiplica")
    public Integer multiplica(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();

        if (numeros.isEmpty() || numeros.size()<2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números para serem multiplicados");
        }
        return matematicaService.multipicla(numeros);
    }

    @PostMapping("divide")
    public Integer divide(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();

        if (numeros.isEmpty() || numeros.size()!=2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário  2 números para serem divididos");
        }

//        for (int i = 0;i < numeros.size(); i++){
//            int primeiroNumero = 0;
//            if (i==0){
//                primeiroNumero = numeros.get(i);
//            }
//            else{
//                if (primeiroNumero < numeros.get(i)){
//                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
//                            "Para dividir o primeiro número da lista deve ser maior que o segundo");
//                }
//            }
        int primeiroNumero = 0;
        for (int numero:numeros){
            if (numeros.indexOf(numero)==0){
                primeiroNumero = numero;
            }
            else{
                if (primeiroNumero < numero){
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Para dividir o primeiro número da lista deve ser maior que o segundo");}
            }

        }
        return matematicaService.divide(numeros);
    }

}
