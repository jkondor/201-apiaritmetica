package com.br.itau.services;

import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MatematicaService {

    public int soma(List<Integer> numeros) {
        int resultado = 0;
        for (int numero: numeros){
            resultado += numero;
        }
        return resultado;
    }

    public int subtrai(List<Integer> numeros) {
        int resultado = 0;

        for(int i = 0; i < numeros.size(); i++){
            if(i == 0){
                resultado = numeros.get(i);
            }
            else {
                resultado = resultado - numeros.get(i);
            }
        }

/*        int resultado = 0;
        for (int numero: numeros){
            if (numeros.indexOf(numero)==0){
                resultado = numero;
            }
            else{
                resultado -= numero;
            }
        }*/
        return resultado;
    }


    public int multipicla(List<Integer> numeros) {
        int resultado = 0;

        for(int i = 0; i < numeros.size(); i++){
            if(i == 0){
                resultado = numeros.get(i);
            }
            else {
                resultado = resultado * numeros.get(i);
            }
        }

/*
        int resultado = 1;
        for (int numero: numeros){
            resultado *= numero;
        }
*/
        return resultado;
    }

    public int divide(List<Integer> numeros) {
        int resultado = 0;

        resultado = numeros.get(0)/ numeros.get(1);

        return resultado;
    }

}
